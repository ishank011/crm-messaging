#  Messaging API for CRM systems

An API service comprising of some basic endpoints that enable interacting with contacts, and sending and receiving messages. The currently supported endpoints include:

- `GET /contacts`: Get a list of all the contacts in the system.
- `POST /contacts`: Add a new contact. The parameters are supposed to be present in the request body.
- `GET /messages`: Get a list of all the conversations in the system.
- `POST /messages`: Send a message to an contact. The parameters are supposed to be present in the request body.
- `POST /messages/receive`: Webhook to receive a message. The parameters are supposed to be present in the request body.

This project uses Quarkus, Postgres for the database backend, Hibernate ORM for the database bindings and Docker for containerizing deployments.

##  Running the application
### In dev mode

You can run your application in dev mode that enables live coding using:
```
./mvnw compile quarkus:dev
```

###  Generating the docker image and using compose
Another way of running the app is to package it and generating the docker image and then the container can be run using `docker compose` (needs docker >= 20.10.6). 
```
./mvnw package
docker build -f src/main/docker/Dockerfile.jvm -t crm-messaging/jvm .
docker-compose up
```

The endpoints are then exposed at port 8080, and can be accessed like
```
curl -v http://localhost:8080/messages
```

## Requests for testing

```
# Add a valid contact, should return 201
curl --location --request POST 'http://localhost:8080/contacts' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Lisa",
    "mail": "lisa@someprovider.com",
    "address": "Rue de Geneve, Paris"
}'

# Add a contact with the same mail as an existing one, should return 409
curl --location --request POST 'http://localhost:8080/contacts' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Lisa",
    "mail": "alice@gmail.com",
    "address": "Rue de Geneve, Paris"
}'

# Add a contact while setting the ID, should return 400
curl --location --request POST 'http://localhost:8080/contacts' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Lisa",
    "mail": "lisa@someprovider.com",
    "address": "Rue de Geneve, Paris",
    "id": 42
}'

# Send a valid message
curl --location --request POST 'http://localhost:8080/messages' \
--header 'Content-Type: application/json' \
--data-raw '{
    "text": "what'\''s up <name>? The BTC price is <BTC>",
    "recipient": {
        "mail": "alice@gmail.com"
    }
}'

# Receive a valid message from a contact
curl --location --request POST 'http://localhost:8080/messages/receive' \
--header 'Content-Type: application/json' \
--data-raw '{
    "text": "hello!",
    "sender": {
        "mail": "bob@yahoo.com"
    }
}'

# Receive a valid message from a person not added to contacts
curl --location --request POST 'http://localhost:8080/messages/receive' \
--header 'Content-Type: application/json' \
--data-raw '{
    "text": "hello!",
    "sender": {
        "mail": "matt@yahoo.com"
    }
}'
```
