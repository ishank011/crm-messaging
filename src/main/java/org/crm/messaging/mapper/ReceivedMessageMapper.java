package org.crm.messaging.mapper;

import org.mapstruct.Mapping;
import org.mapstruct.Mapper;
import org.crm.messaging.dto.ReceivedMessageDto;
import org.crm.messaging.model.Message;

@Mapper(config = MappingConfig.class)
public interface ReceivedMessageMapper {
    /*
     * @param message entity to be mapped
     * 
     * @return mapped dto
     */
    @Mapping(target = "sender", source = "contact")
    @Mapping(target = "sender.id", source = "contact.id")
    ReceivedMessageDto toResource(Message message);

    /*
     * @param messageDto to be mapped
     * 
     * @return mapped entity
     */
    @Mapping(target = "contact", source = "sender")
    @Mapping(target = "contact.id", source = "sender.id")
    @Mapping(target = "isReceived", constant = "true")
    Message fromResource(ReceivedMessageDto messageDto);
}