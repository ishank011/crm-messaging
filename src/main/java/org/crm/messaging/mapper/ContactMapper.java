package org.crm.messaging.mapper;

import org.mapstruct.Mapper;
import org.crm.messaging.dto.ContactDto;
import org.crm.messaging.model.Contact;

@Mapper(config = MappingConfig.class)
public interface ContactMapper {
    /*
     * @param contact entity to be mapped
     * 
     * @return mapped dto
     */
    ContactDto toResource(Contact contact);

    /*
     * @param contactDto to be mapped
     * 
     * @return mapped entity
     */
    Contact fromResource(ContactDto contactDto);
}