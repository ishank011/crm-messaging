package org.crm.messaging.mapper;

import org.mapstruct.MapperConfig;

@MapperConfig(componentModel = "cdi")
interface MappingConfig {
}