package org.crm.messaging.mapper;

import org.mapstruct.Mapping;
import org.mapstruct.Mapper;
import org.crm.messaging.dto.SentMessageDto;
import org.crm.messaging.model.Message;

@Mapper(config = MappingConfig.class)
public interface SentMessageMapper {
    /*
     * @param message entity to be mapped
     * 
     * @return mapped dto
     */
    @Mapping(target = "recipient", source = "contact")
    @Mapping(target = "recipient.id", source = "contact.id")
    SentMessageDto toResource(Message message);

    /*
     * @param messageDto to be mapped
     * 
     * @return mapped entity
     */
    @Mapping(target = "contact", source = "recipient")
    @Mapping(target = "contact.id", source = "recipient.id")
    @Mapping(target = "isReceived", constant = "false")
    Message fromResource(SentMessageDto messageDto);
}