package org.crm.messaging.resources;

import org.crm.messaging.model.Contact;
import org.crm.messaging.dto.ContactDto;
import org.crm.messaging.mapper.ContactMapper;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import java.util.stream.Collectors;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Path("/contacts")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class ContactResource {
    @Inject
    ContactMapper contactMapper;
    private static final Logger LOGGER = Logger.getLogger(ContactResource.class.getName());

    @GET
    public List<ContactDto> findAll() {
        return Contact.<Contact>find("knownContact", true).stream().map(c -> contactMapper.toResource(c))
                .collect(Collectors.toList());
    }

    @POST
    @Transactional
    public Response create(ContactDto contact) {
        // Return bad request if caller tries to set the ID of the contact
        if (contact.getId() != null) {
            throw new WebApplicationException("cannot set the id of the contact", 400);
        }
        
        // Mail has to be set to create a new contact
        if (contact.getMail() == null) {
            throw new WebApplicationException("contact's mail not specified", 400);
        }
        // Return a conflict if a contact with the same mail already exists
        if (Contact.<Contact>find("mail", contact.getMail()).firstResult() != null) {
            throw new WebApplicationException("contact with the specified mail already exists", 409);
        }

        var c = contactMapper.fromResource(contact);
        c.setKnownContact(true);
        c.persist();
        return Response.ok(contactMapper.toResource(c)).status(201).build();
    }

    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {

        @Inject
        ObjectMapper objectMapper;

        @Override
        public Response toResponse(Exception exception) {
            LOGGER.error("Failed to handle request", exception);

            int code = 500;
            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }

            ObjectNode exceptionJson = objectMapper.createObjectNode();
            exceptionJson.put("exceptionType", exception.getClass().getName());
            exceptionJson.put("code", code);

            if (exception.getMessage() != null) {
                exceptionJson.put("error", exception.getMessage());
            }

            return Response.status(code).entity(exceptionJson).build();
        }

    }
}
