package org.crm.messaging.resources;

import org.crm.messaging.model.Message;
import org.crm.messaging.dto.ReceivedMessageDto;
import org.crm.messaging.dto.SentMessageDto;
import org.crm.messaging.mapper.ReceivedMessageMapper;
import org.crm.messaging.mapper.SentMessageMapper;
import org.crm.messaging.mapper.ContactMapper;
import org.crm.messaging.model.Contact;
import org.crm.messaging.dto.ContactDto;

import org.crm.messaging.templates.TemplateHandler;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.Date;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import org.jboss.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Path("/messages")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class MessageResource {
    @Inject
    SentMessageMapper sentMessageMapper;
    @Inject
    ReceivedMessageMapper receivedMessageMapper;
    @Inject
    ContactMapper contactMapper;
    @Inject
    TemplateHandler templateHandler;
    private static final Logger LOGGER = Logger.getLogger(MessageResource.class.getName());

    @GET
    public Response findAll() {
        List<SentMessageDto> sent = Message.<Message>find("isReceived", false).stream()
                .map(c -> sentMessageMapper.toResource(c)).collect(Collectors.toList());
        List<ReceivedMessageDto> received = Message.<Message>find("isReceived", true).stream()
                .map(c -> receivedMessageMapper.toResource(c)).collect(Collectors.toList());

        Map<String, Object> map = new HashMap<>();
        map.put("received_messages", received);
        map.put("sent_messages", sent);

        return Response.ok(map).build();
    }

    @POST
    @Transactional
    public Response sendMessage(SentMessageDto message) {
        // Return bad request if caller tries to set the ID of the message
        if (message.getId() != null) {
            throw new WebApplicationException("cannot set the id of the message", 400);
        }
        
        // Mail of the recipient has to be set to send a message
        ContactDto dto = message.getRecipient();
        if (dto == null || dto.getMail() == null) {
            throw new WebApplicationException("recipient cannot be null", 400);
        }
        if (dto.getId() != null) {
            throw new WebApplicationException("cannot set the id of the recipient", 400);
        }

        // The recipient should be added to the contacts
        Contact recipient = Contact.<Contact>find("mail = ?1 and knownContact = ?2", dto.getMail(), true).firstResult();
        if (recipient == null) {
            throw new WebApplicationException("contact with the specified mail does not exist", 404);
        }

        message.setText(templateHandler.ApplyTemplate(message.getText(), recipient));
        message.setCreated(new Date());
        var m = sentMessageMapper.fromResource(message);
        m.setContact(recipient);
        m.persist();
        return Response.ok(sentMessageMapper.toResource(m)).status(201).build();
    }

    @POST
    @Path("receive")
    @Transactional
    public Response receiveMessage(ReceivedMessageDto message) {
        // Return bad request if caller tries to set the ID of the message
        if (message.getId() != null) {
            throw new WebApplicationException("cannot set the id of the message", 400);
        }
        
        // Mail of the sender has to be set to send a message
        ContactDto dto = message.getSender();
        if (dto == null || dto.getMail() == null) {
            throw new WebApplicationException("sender cannot be null", 400);
        }
        if (dto.getId() != null) {
            throw new WebApplicationException("cannot set the id of the sender", 400);
        }

        Contact sender = Contact.<Contact>find("mail", dto.getMail()).firstResult();
        if (sender == null) {
            sender = contactMapper.fromResource(dto);
        }

        message.setCreated(new Date());
        var m = receivedMessageMapper.fromResource(message);
        m.setContact(sender);
        m.persist();
        return Response.ok(receivedMessageMapper.toResource(m)).status(201).build();
    }

    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {

        @Inject
        ObjectMapper objectMapper;

        @Override
        public Response toResponse(Exception exception) {
            LOGGER.error("Failed to handle request", exception);

            int code = 500;
            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }

            ObjectNode exceptionJson = objectMapper.createObjectNode();
            exceptionJson.put("exceptionType", exception.getClass().getName());
            exceptionJson.put("code", code);

            if (exception.getMessage() != null) {
                exceptionJson.put("error", exception.getMessage());
            }

            return Response.status(code).entity(exceptionJson).build();
        }

    }
}
