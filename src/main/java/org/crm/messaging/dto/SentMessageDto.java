package org.crm.messaging.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import java.util.Date;

@RegisterForReflection
public class SentMessageDto {
    private Long id;
    private String text;
    private ContactDto recipient;
    private Date created;

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public ContactDto getRecipient() {
        return recipient;
    }

    public Date getCreated() {
        return created;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setRecipient(ContactDto recipient) {
        this.recipient = recipient;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}