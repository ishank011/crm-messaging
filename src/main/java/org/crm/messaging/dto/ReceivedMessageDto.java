package org.crm.messaging.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import java.util.Date;

@RegisterForReflection
public class ReceivedMessageDto {
    private Long id;
    private String text;
    private ContactDto sender;
    private Date created;

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public ContactDto getSender() {
        return sender;
    }

    public Date getCreated() {
        return created;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setSender(ContactDto sender) {
        this.sender = sender;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}