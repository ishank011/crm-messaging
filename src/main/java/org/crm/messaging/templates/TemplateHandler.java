package org.crm.messaging.templates;

import org.stringtemplate.v4.*;

import org.crm.messaging.model.Contact;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import javax.enterprise.inject.Model;
import org.jboss.logging.Logger;

@Model
public class TemplateHandler {

    private String bitcoinAPIURI = "https://blockchain.info/ticker";
    private static final Logger LOGGER = Logger.getLogger(TemplateHandler.class.getName());

    private static String getBitcoinPrice(String uri) throws Exception {
        var url = new URL(uri);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.connect();

        var inline = "";
        if (conn.getResponseCode() != 200)
            throw new RuntimeException("HttpResponseCode: " + conn.getResponseCode());
        else {
            Scanner sc = new Scanner(url.openStream());
            while (sc.hasNext()) {
                inline += sc.nextLine();
            }
            sc.close();
        }

        JSONParser parser = new JSONParser();
        JSONObject jsonObj = (JSONObject) parser.parse(inline);
        JSONObject usdJsonObj = (JSONObject) jsonObj.get("USD");
        return "$" + Double.toString((Double) usdJsonObj.get("last"));

    }

    private void contactTemplate(ST query, Contact c) {
        query.add("name", c.getName());
        query.add("address", c.getAddress());
    }

    private void bitcoinTemplate(ST query) {
        String btcPrice = "";
        try {
            btcPrice = getBitcoinPrice(bitcoinAPIURI);
        } catch (Exception e) {
            LOGGER.error("Couldn't get BTC price, leaving it empty", e);
        }
        query.add("BTC", btcPrice);
    }

    public String ApplyTemplate(String text, Contact c) {
        ST query = new ST(text);
        contactTemplate(query, c);
        bitcoinTemplate(query);
        return query.render();
    }
}
