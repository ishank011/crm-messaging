package org.crm.messaging.model;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
@Cacheable
public class Contact extends PanacheEntity {

    @Column(length = 40)
    private String name;
    @Column(length = 40, unique = true)
    private String mail;
    @Column(length = 100)
    private String address;
    @Column
    private boolean knownContact;

    public Contact() {
    }

    public Contact(String name, String mail, String address, boolean knownContact) {
        this.name = name;
        this.mail = mail;
        this.address = address;
        this.knownContact = knownContact;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getAddress() {
        return address;
    }

    public boolean getKnownContact() {
        return knownContact;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setKnownContact(boolean knownContact) {
        this.knownContact = knownContact;
    }
}
