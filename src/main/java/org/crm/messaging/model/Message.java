package org.crm.messaging.model;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.Date;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
@Cacheable
public class Message extends PanacheEntity {

    @Column(length = 200)
    private String text;
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "contact_id")
    private Contact contact;
    @Column
    private Date created;
    @Column
    private boolean isReceived;

    public Message() {
    }

    public Message(String text, Contact contact, boolean isReceived) {
        this.text = text;
        this.contact = contact;
        this.isReceived = isReceived;
        this.created = new Date();
    }

    public String getText() {
        return text;
    }

    public Contact getContact() {
        return contact;
    }

    public Date getCreated() {
        return created;
    }

    public boolean getisReceived() {
        return isReceived;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public void setIsReceived(boolean isReceived) {
        this.isReceived = isReceived;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
