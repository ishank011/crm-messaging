INSERT INTO contact(id, name, mail, address, knownContact) VALUES (nextval('hibernate_sequence'), 'Alice', 'alice@gmail.com', 'Berlin', true);
INSERT INTO contact(id, name, mail, address, knownContact) VALUES (nextval('hibernate_sequence'), 'Bob', 'bob@yahoo.com', 'Geneve', true);
INSERT INTO contact(id, name, mail, address, knownContact) VALUES (nextval('hibernate_sequence'), 'Charles', 'charles@gmail.com', 'Munich', true);

INSERT INTO message(id, text, contact_id, isReceived, created) VALUES (nextval('hibernate_sequence'), 'Hello Alice', 1, false, NOW());
INSERT INTO message(id, text, contact_id, isReceived, created) VALUES (nextval('hibernate_sequence'), 'Howdy?', 2, true, NOW());
