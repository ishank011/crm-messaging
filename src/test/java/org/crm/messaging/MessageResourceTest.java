package org.crm.messaging;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import io.restassured.response.Response;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

@QuarkusTest
public class MessageResourceTest {

    @Test
    public void testGetCall() {
        given().when().get("/messages").then().statusCode(200).body(containsString("Hello Alice"),
                containsString("Howdy?"));
    }

    @Test
    public void testCorrectPostCall() {
        String requestBody = "{\n" +
            "  \"text\": \"what's up?\",\n" +
            "  \"recipient\": {\n" +
            "    \"mail\": \"alice@gmail.com\"\n" +
            "  }\n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/messages").then().extract().response();
        var recipient = response.jsonPath().getMap("recipient");
        Assertions.assertEquals(201, response.statusCode());
        Assertions.assertEquals("what's up?", response.jsonPath().getString("text"));
        Assertions.assertEquals(1, (Integer) recipient.get("id"));
        Assertions.assertEquals("alice@gmail.com", (String) recipient.get("mail"));
        Assertions.assertEquals("Alice", (String) recipient.get("name"));
        Assertions.assertEquals("Berlin", (String) recipient.get("address"));
    }

    @Test
    public void testBadMessageRequestPostCall() {
        String requestBody = "{\n" +
            "  \"text\": \"what's up?\",\n" +
            "  \"id\": \"42\",\n" +
            "  \"recipient\": {\n" +
            "    \"mail\": \"alice@gmail.com\"\n" +
            "  }\n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/messages").then().extract().response();
        Assertions.assertEquals(400, response.statusCode());
    }

    @Test
    public void testBadRecipientRequestPostCall() {
        String requestBody = "{\n" +
            "  \"text\": \"what's up?\",\n" +
            "  \"recipient\": {\n" +
            "    \"mail\": \"alice@gmail.com\",\n" +
            "    \"id\": \"1\"\n" +
            "  }\n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/messages").then().extract().response();
        Assertions.assertEquals(400, response.statusCode());
    }

    @Test
    public void testMissingRecipientRequestPostCall() {
        String requestBody = "{\n" +
            "  \"text\": \"what's up?\",\n" +
            "  \"recipient\": {\n" +
            "    \"id\": \"1\"\n" +
            "  }\n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/messages").then().extract().response();
        Assertions.assertEquals(400, response.statusCode());
    }

    @Test
    public void testNonExistingRecipientRequestPostCall() {
        String requestBody = "{\n" +
            "  \"text\": \"what's up?\",\n" +
            "  \"recipient\": {\n" +
            "    \"mail\": \"noone@gmail.com\"\n" +
            "  }\n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/messages").then().extract().response();
        Assertions.assertEquals(404, response.statusCode());
    }

    @Test
    public void testCorrectReceivedPostCall() {
        String requestBody = "{\n" +
            "  \"text\": \"what's up?\",\n" +
            "  \"sender\": {\n" +
            "    \"mail\": \"alice@gmail.com\"\n" +
            "  }\n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/messages/receive").then().extract().response();
        var recipient = response.jsonPath().getMap("sender");
        Assertions.assertEquals(201, response.statusCode());
        Assertions.assertEquals("what's up?", response.jsonPath().getString("text"));
        Assertions.assertEquals(1, (Integer) recipient.get("id"));
        Assertions.assertEquals("alice@gmail.com", (String) recipient.get("mail"));
        Assertions.assertEquals("Alice", (String) recipient.get("name"));
        Assertions.assertEquals("Berlin", (String) recipient.get("address"));
    }

    @Test
    public void testCorrectReceivedFromUnknownSenderPostCall() {
        String requestBody = "{\n" +
            "  \"text\": \"what's up?\",\n" +
            "  \"sender\": {\n" +
            "    \"mail\": \"unknown@gmail.com\"\n" +
            "  }\n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/messages/receive").then().extract().response();
        var recipient = response.jsonPath().getMap("sender");
        Assertions.assertEquals(201, response.statusCode());
        Assertions.assertEquals("what's up?", response.jsonPath().getString("text"));
        Assertions.assertEquals("unknown@gmail.com", (String) recipient.get("mail"));
    }

    @Test
    public void testBadMessageRequestReceivedPostCall() {
        String requestBody = "{\n" +
            "  \"text\": \"what's up?\",\n" +
            "  \"id\": \"42\",\n" +
            "  \"sender\": {\n" +
            "    \"mail\": \"alice@gmail.com\"\n" +
            "  }\n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/messages/receive").then().extract().response();
        Assertions.assertEquals(400, response.statusCode());
    }

    @Test
    public void testBadRecipientRequestReceivedPostCall() {
        String requestBody = "{\n" +
            "  \"text\": \"what's up?\",\n" +
            "  \"sender\": {\n" +
            "    \"mail\": \"alice@gmail.com\",\n" +
            "    \"id\": \"1\"\n" +
            "  }\n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/messages/receive").then().extract().response();
        Assertions.assertEquals(400, response.statusCode());
    }

    @Test
    public void testMissingRecipientRequestReceivedPostCall() {
        String requestBody = "{\n" +
            "  \"text\": \"what's up?\",\n" +
            "  \"sender\": {\n" +
            "    \"id\": \"1\"\n" +
            "  }\n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/messages/receive").then().extract().response();
        Assertions.assertEquals(400, response.statusCode());
    }

}