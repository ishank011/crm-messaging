package org.crm.messaging;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import io.restassured.response.Response;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

@QuarkusTest
public class ContactResourceTest {

    @Test
    public void testGetCall() {
        given().when().get("/contacts").then().statusCode(200).body(containsString("Alice"), containsString("Bob"),
                containsString("Charles"));
    }

    @Test
    public void testCorrectPostCall() {
        String requestBody = "{\n" +
            "  \"name\": \"Lisa\",\n" +
            "  \"mail\": \"lisa@someprovider.com\",\n" +
            "  \"address\": \"Rue de Geneve, Paris\" \n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/contacts").then().extract().response();
        Assertions.assertEquals(201, response.statusCode());
        Assertions.assertEquals("Lisa", response.jsonPath().getString("name"));
        Assertions.assertEquals("lisa@someprovider.com", response.jsonPath().getString("mail"));
        Assertions.assertEquals("Rue de Geneve, Paris", response.jsonPath().getString("address"));
    }

    @Test
    public void testConflictPostCall() {
        String requestBody = "{\n" +
            "  \"name\": \"Lisa\",\n" +
            "  \"mail\": \"alice@gmail.com\",\n" +
            "  \"address\": \"Rue de Geneve, Paris\" \n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/contacts").then().extract().response();
        Assertions.assertEquals(409, response.statusCode());
    }

    @Test
    public void testBadRequestPostCall() {
        String requestBody = "{\n" +
            "  \"name\": \"Lisa\",\n" +
            "  \"mail\": \"newid@gmail.com\",\n" +
            "  \"id\": \"42\",\n" +
            "  \"address\": \"Rue de Geneve, Paris\" \n}";
        Response response = given().header("Content-type", "application/json").and().body(requestBody).when().post("/contacts").then().extract().response();
        Assertions.assertEquals(400, response.statusCode());
    }
}